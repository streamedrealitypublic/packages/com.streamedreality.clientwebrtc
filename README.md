# Streamed Reality XR Client WebRTC Documentation

**Version:** v1.0.0  
**Authors:** Streamed Reality  
**Target:** Android

## Description
Streamed Reality Client WebRTC facilitates the use of WebRTC for the Streamed Reality Client SDK, which enables the conversion of existing or new XR Unity applications for cloud or local computer deployment.

## Quick Start Guide
Please see the README.md in Streamed Reality Client SDK.